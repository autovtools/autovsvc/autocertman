FROM alpine:latest

ARG STEP_TGZ

RUN apk add --no-cache docker-cli ;\
    wget "${STEP_TGZ}" ;\
    tar xf step_linux_* ;\
    mv step_*/bin/step /usr/bin/ ;\ 
    rm -rf step_*

COPY ./deploy.sh /deploy.sh
COPY ./entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
