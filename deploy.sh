#!/bin/sh

# Usage: /deploy.sh myservice /path/to/stack.yml /certs/myservice

cat << EOF


#################################################################################################



EOF
# Name of the service to deploy
SVC="${1}"
# The yml stack file containing the service
STACK="${2}"
# Path to certificate
SECRETS_DIR="${3}"

function usage(){
    echo "Usage: ${0} /path/to/myservice/stack.yml /certs/myservice"
    exit 1
}

function source_env_file(){
    if [ "${1}" = "" ]; then
        echo "Usage: source_env_file /path/to/file.env" 
    fi
    echo "Sourcing environment from ${1}"
    # envsubt is safer, but requires gettext package
    export $(eval "echo $(cat "${1}"| xargs)")
}

if [ "$#" -ne 3 ]; then
    usage
fi

ENV_FILE="${STACK%.*}.env"
if [ -f "${ENV_FILE}" ]; then
    source_env_file "${ENV_FILE}"
fi

DEPLOY_DIR="$(dirname "${STACK}")"
for CERT in $(find "${SECRETS_DIR}" -type f -name "*.crt"); do
    FINGERPRINT="$(step certificate fingerprint "${CERT}" | head -c 16)"
    KEY="${CERT%.*}.key"
    # Create any secrets the service needs
    for SECRET_FILE in "${CERT}" "${KEY}"; do
        SECRET_NAME="$(basename "${SECRET_FILE}").${FINGERPRINT}"
        echo "[$(date -Iseconds)] Creating secret ${SECRET_NAME} for service ${SVC}"
        docker secret create "${SECRET_NAME}" "${SECRET_FILE}"
    done
done

# no pushd in alpine
cd "${DEPLOY_DIR}"
echo "[$(date -Iseconds)] Redeploying ${SVC}..."
REGISTRY=${REGISTRY} FINGERPRINT=${FINGERPRINT} docker stack deploy -c "${STACK}" "${SVC}"


# Attach stunnel, if requested
# i.e. a service contains a file like:
#   stunnel.STUNNEL_BASE_ENV.attach[.OPTIONAL_LABEL]
STUNNEL_ATTACH="$(find $(pwd) -name "stunnel.*.attach*")"
STUNNEL_SERVICE_DIR="../stunnel"
cd "${STUNNEL_SERVICE_DIR}"
for STUNNEL_ATTACH in ${STUNNEL_ATTACH}; do
    # Service needs an stunnel side-car
    # Load all CA_* variables, since we need to issue a cert
    source_env_file "${HOME}/ca.config.env"
    # Load STUNNEL_SVC (and any variables needed by stack.yml)
    source_env_file "${STUNNEL_ATTACH}"
    echo "Deploying stunnel side-car using ${STUNNEL_ATTACH}"

    STUNNEL_BASE_CONFIG="$(basename "${STUNNEL_ATTACH}")"
    STUNNEL_SVC_SUFFIX="${STUNNEL_BASE_CONFIG##*.}"
    export STUNNEL_BASE_ENV="${STUNNEL_BASE_CONFIG%.attach*}.env"
    #source_env_file "${STUNNEL_BASE_ENV}"
    STUNNEL_SVC_FULL="stunnel_${STUNNEL_SVC}_${STUNNEL_SVC_SUFFIX}"
    CERTS="/certs/${STUNNEL_SVC_FULL}/${STUNNEL_BASE_CONFIG}"
    mkdir -p "${CERTS}"

    # Issue cert for stunnel + set up renewal
    CERT="${CERTS}/stunnel_${STUNNEL_SVC}.crt"
    KEY="${CERTS}/stunnel_${STUNNEL_SVC}.key"
    FQDN="${STUNNEL_SVC}.${CA_DOMAIN}"
    "${HOME}/issue-cert" "${FQDN}" "${CERT}" "${KEY}"
    DEPLOY="${0} ${STUNNEL_SVC_FULL} $(readlink -f stack.yml) ${CERTS}"
    echo "Deploying ${STUNNEL_SVC_FULL} ..."
    ${DEPLOY}
    echo "Autorenewing cert for ${STUNNEL_SVC_FULL} ..."
    step ca renew --force --daemon --ca-url "${CA_URL}" "${CERT}" "${KEY}" --exec "${DEPLOY}" &
done
