#!/bin/sh

# NOT_AFTER can be overridden to change how long certs are valid
# Renewal happens before 2/3 of the valid life has passed, but keep in mind:
#   Shorter is generally more secure
#   Your certs may temporarily lapse if you don't keep 100% uptime
# See '--not-after': https://smallstep.com/docs/cli/ca/certificate/
NOT_AFTER=${NOT_AFTER:-720h}
# (tmpfs / in memory)
CERTS_TMP=${CERTS_TMP:-/certs}

# mount source on host; pass through to children who need it
export SERVICE_DIR="${SERVICE_DIR}"
if [ "$(id -u)" = "0" ]; then
    # Make sure our low-priv user can run docker commands,
    #   then switch to them
    # https://github.com/jenkinsci/docker/issues/196#issuecomment-179486312
    # https://vitorbaptista.com/how-to-access-hosts-docker-socket-without-root

    SOCK="/var/run/docker.sock"
    GRP="docker"
    USR="step"

    adduser -S "${USR}"
    if [ -S "${SOCK}" ]; then
        GID=$(stat -c '%g' "${SOCK}")
        addgroup -g "${GID}" "${GRP}"
        addgroup "${USR}" "${GRP}"
    fi

    # Re-exec as low-priv user
    su -s /bin/sh "${USR}" -c "$0 $@"
else
    # Actual entry-point
    AUTOSTEPCA_EXPORT="/autostepca_export"
    #ROOT_CRT="${HOME}/.step/certs/root_ca.crt"
    ROOT_CRT="/autostepca_export/root_ca.crt"

    ISSUER="autocertman"
    TOKEN_PASS="${AUTOSTEPCA_EXPORT}/autocertman.pass"

    while true; do
        if [ -f "${ROOT_CRT}" ]; then
           break
        else
            echo "[$(date -Iseconds)] Waiting for autostepa initialization..."
            sleep 10
        fi
    done

    CA_URL="$(cat "${AUTOSTEPCA_EXPORT}/ca.url")"
    CA_FINGERPRINT="$(cat "${AUTOSTEPCA_EXPORT}/ca.fingerprint")"

    # Extract domain name from CA_URL
    #   (Busybox does not have --complement)
    CA_FQDN="$(echo $CA_URL | cut -d '/' -f3 | cut -d ':' -f1)"
    CA_HOSTNAME="$(echo $CA_FQDN | cut -d '.' -f 1)"
    CA_DOMAIN=$(echo "${CA_FQDN}" | sed "s/${CA_HOSTNAME}\.//")
    # Write the CA environment to a file where it can be sourced by others
    CA_CONFIG_ENV="${HOME}/ca.config.env"
    cat << EOF > "${CA_CONFIG_ENV}"
CA_URL="${CA_URL}"
CA_FINGERPRINT="${CA_FINGERPRINT}"
CA_FQDN="${CA_FQDN}"
CA_HOSTNAME="${CA_HOSTNAME}"
CA_DOMAIN="${CA_DOMAIN}"
EOF
    chmod 0400 "${CA_CONFIG_ENV}"
    export $(eval "echo $(cat "${CA_CONFIG_ENV}"| xargs)")
    echo "[$(date -Iseconds)] Waiting for ${CA_URL} to be reachable..."
    while true; do
        STATUS="$(step ca health --ca-url "${CA_URL}" --root "${ROOT_CRT}" 2>&1)"
        if [ "${STATUS}" = "ok" ]; then
            break
        else
            date -Iseconds
            echo "${STATUS}"
            sleep 10
        fi
    done

    step ca bootstrap --ca-url "${CA_URL}"  --fingerprint "${CA_FINGERPRINT}"
    # Generate a helper script that can be used interactively to issue certs
    ISSUE_CERT="${HOME}/issue-cert"
    # Pass variables from entrypoint by allowing variable substitution
    cat << EOF > "${ISSUE_CERT}" 
#!/bin/sh
ISSUER="${ISSUER}"
TOKEN_PASS="${TOKEN_PASS}"
CA_URL="${CA_URL}"
NOT_AFTER="${NOT_AFTER}"
STEP_USER="${USER}"
EOF
    # Write the rest of the script without substitution
    cat << 'EOF' >> "${ISSUE_CERT}" 
function usage() {
    echo "Usage: $0 FQDN CERT_FILE KEY_FILE"
    exit 1
}
FQDN="${1}"
CERT="${2}"
KEY="${3}"
for ARG in "${FQDN}" "${CERT}" "${KEY}"; do
    if [ "${ARG}" = "" ] || [ "${ARG}" = "-h" ] || [ "${ARG}" = "--help" ]; then
        usage
    fi
done

# step stores some CA settings in home directory, so make sure we run as step, not root
if [ "${USER}" != "${STEP_USER}" ]; then
    echo Running as "${STEP_USER}..."
    CMD="${0} $@"
    su -s /bin/sh "${STEP_USER}" -c "${CMD}"
else
    TOKEN=$(step ca token "${FQDN}" --issuer "${ISSUER}" --password-file "${TOKEN_PASS}" --ca-url "${CA_URL}" )
    step ca certificate --force --not-after "${NOT_AFTER}" --token "${TOKEN}" --ca-url "${CA_URL}" "${FQDN}" "${CERT}" "${KEY}"
fi
EOF

    chmod 0500 "${ISSUE_CERT}"

    for CERT in $(find "${SERVICE_DIR}" -name "*.crt"); do
        BASE=$(basename "${CERT}")
        DEST_DIR="${CERTS_TMP}/${BASE%.*}"
        DIR=$(dirname "${CERT}")
        SVC="$(echo ${BASE} | awk -F '.' '{print $1}')"
        KEY="${DIR}/${SVC}.key"
        FQDN="${SVC}.${CA_DOMAIN}"
        STACK="${DIR}/../stack.yml"

        mkdir -p "${DEST_DIR}"
        if [ ! "$(ls -A "${DEST_DIR}")" ]; then
            cp "${KEY}" "${DEST_DIR}"
            cp "${CERT}" "${DEST_DIR}"
        fi
        KEY="${DEST_DIR}/${SVC}.key"
        CERT="${DEST_DIR}/${SVC}.crt"
        # Lock down permissions
        chmod 0700 "${DEST_DIR}"
        chmod 0600 "${KEY}"
        chmod 0600 "${CERT}"
        
        DEPLOY="/deploy.sh ${SVC} ${STACK} ${DEST_DIR}"
        if ! step certificate verify --roots "${ROOT_CRT}" "${CERT}" ; then
            # If cert if invalid (expired) or does not exist, issue a new one
            echo "${CERT} is missing or invalid; generating a cert for ${FQDN}"
            #TOKEN=$(step ca token "${FQDN}" --issuer "${ISSUER}" --password-file "${TOKEN_PASS}" --ca-url "${CA_URL}" )
            #step ca certificate --force --not-after "${NOT_AFTER}" --token "${TOKEN}" --ca-url "${CA_URL}" "${FQDN}" "${CERT}" "${KEY}"
            "${ISSUE_CERT}" "${FQDN}" "${CERT}" "${KEY}"
            echo "Redeploying ${SVC}"
            ${DEPLOY}
        fi
        echo "Automatically renewing ${CERT} for service ${SVC}"
        step ca renew --force --daemon --ca-url "${CA_URL}" "${CERT}" "${KEY}" --exec "${DEPLOY}" &
    done
    echo "Done"
    sleep infinity
fi
