#!/bin/bash

# Get latest step-cli release
URL="$(curl -s https://github.com/smallstep/cli/releases/latest | awk -F'"' '{print $2}' | sed 's/tag/download/')";
TGZ="step_linux$(echo $URL | awk -F '/' '{print $8}' | tr 'v' '_')_amd64.tar.gz" ; 
STEP_TGZ="${URL}/${TGZ}"

docker build --build-arg STEP_TGZ="${STEP_TGZ}" --tag autocertman .
